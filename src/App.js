import React, { useLayoutEffect, useRef } from "react";
import "./App.css";

import { Machine, spawn, sendParent, actions, send, assign } from "xstate";
import { assign as assignImmer } from "@xstate/immer";
import { useActor, useMachine } from "@xstate/react";

const { pure } = actions;

const setAllChildren = ({ checked }) => [
  pure((ctx) =>
    ctx.children?.map(({ ref }) =>
      send({ type: checked ? "TICK" : "UNTICK", fromParent: true }, { to: ref })
    )
  ),
  assignImmer((ctx) =>
    ctx.children?.forEach((c) => {
      ctx.valueAcc[c.value] = checked;
    })
  ),
];

const maybeSendChildrenChanged = ({ checked, isRoot }) =>
  pure(
    (ctx, e) =>
      !isRoot &&
      sendParent({
        type: "CHILDREN_CHANGED",
        updateParent: !e.fromParent,
        items: {
          ...ctx.valueAcc,
          [ctx.value]: checked,
        },
      })
  );

const createStateSpec = ({ checked, isRoot }) => ({
  entry: [
    maybeSendChildrenChanged({ checked, isRoot }),
    ...(isRoot
      ? [assignImmer((ctx) => (ctx.valueAcc[ctx.value] = checked))]
      : []),
  ],
  on: {
    [checked ? "UNTICK" : "TICK"]: {
      target: checked ? "empty" : "checked",
      actions: setAllChildren({ checked: !checked }),
    },
  },
});

const createCheckboxTreeMachine = ({
  id = "checkboxTree",
  tree,
  isRoot = true,
}) =>
  Machine(
    {
      id,
      initial: "initializing",
      context: {
        children: [],
        value: tree?.value,
        valueAcc: {},
      },
      states: {
        initializing: {
          entry: assign({
            children: () =>
              tree?.children?.map((c) => ({
                value: c.value,
                ref: spawn(
                  createCheckboxTreeMachine({
                    id: `checkbox_${c.value}`,
                    tree: c,
                    isRoot: false,
                  })
                ),
              })),
          }),
          always: tree?.checked ? "checked" : "empty",
        },
        deducing: {
          always: [
            {
              cond: "allChildrenChecked",
              target: "checked",
            },
            {
              cond: "someChildrenChecked",
              target: "indeterminate",
            },
            "empty",
          ],
        },
        checked: createStateSpec({ checked: true, isRoot }),
        empty: createStateSpec({ checked: false, isRoot }),
        indeterminate: createStateSpec({ checked: false, isRoot }),
      },
      on: {
        CHILDREN_CHANGED: [
          {
            cond: (ctx, e) => e.updateParent,
            actions: "updateValueAcc",
            target: "deducing",
          },
          {
            actions: "updateValueAcc",
          },
        ],
      },
    },
    {
      actions: {
        updateValueAcc: assign({
          valueAcc: (ctx, e) => ({
            ...ctx.valueAcc,
            ...(e?.items ?? {}),
          }),
        }),
      },
      guards: {
        someChildrenChecked: (ctx) =>
          ctx.children?.some(({ value }) => ctx.valueAcc[value]),
        allChildrenChecked: (ctx) =>
          ctx.children?.every(({ value }) => ctx.valueAcc[value]),
      },
    }
  );

const Checkbox = ({ value, state, label, onChange }) => {
  const ref = useRef(null);

  useLayoutEffect(() => {
    if (ref.current) {
      ref.current.indeterminate = state === "indeterminate";
    }
  }, [state, ref]);

  const checked = state === "checked";
  return (
    <label>
      <input
        ref={ref}
        value={value}
        type="checkbox"
        checked={checked}
        onClick={onChange}
      />
      {label}
    </label>
  );
};

const DeeperCheckboxBranch = ({ treeActor }) => {
  const [state, send] = useActor(treeActor);
  return <CheckboxPresentation state={state} send={send} />;
};

const CheckboxPresentation = ({ state, send }) => (
  <ul style={{ listStyleType: "none" }}>
    <li>
      <Checkbox
        value={state?.context?.value}
        label={state?.context?.value}
        state={state?.value}
        onChange={(e) => {
          send({ type: e?.currentTarget.checked ? "TICK" : "UNTICK" });
        }}
      />
    </li>
    <li>
      <ul style={{ listStyleType: "none" }}>
        {state?.context?.children?.map(({ ref }) => (
          <li>
            <DeeperCheckboxBranch treeActor={ref} />
          </li>
        ))}
      </ul>
    </li>
  </ul>
);

const CheckboxTree = ({ tree }) => {
  const [state, send] = useMachine(
    createCheckboxTreeMachine({ id: `checkbox_${tree?.value}`, tree }),
    {
      devTools: true,
    }
  );

  console.log(state.context.valueAcc);

  return <CheckboxPresentation state={state} send={send} />;
};

const tree = {
  value: "root1",
  children: [
    {
      value: "child1.1",
    },
    {
      value: "child1.2",
    },
    {
      value: "child1.3",
      children: [
        {
          value: "child1.3.1",
        },
        {
          value: "child1.3.2",
        },
      ],
    },
  ],
};

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <CheckboxTree tree={tree} />
      </header>
    </div>
  );
}

export default App;
